/**
 * CONTROLERS
 * slider0 -> stepSize: control the pixel gap used, min: 5 max: 50 step 1
 * slider1 -> panX: control the capture on X axis, min: -width max: +width step 1
 * slider2 -> panY: control the capture on Y axis, min: -width max: +width step 1
 * slider3 -> panZ: control the capture on Z axis, min: -width max: +width step 1
 * slider4 -> pixelDepth: control the depth pixel of the lightness points, min: -250 max: +250
 * slider5 -> meshSize: control the mesh Size for dot mode, min: -250 max: +250
 * slider6 -> strokeWeight: control the stroke weight, min: 1 max: 25
 * 
 * checkBox1 -> dot mode
 * checkBox2 -> line X
 * checkBox3 -> line Y
 * checkBox4 -> Quad mode
 * checkBox5 -> Quad fill
 * checkBox6 -> Cube mode fill
 * 
 */


let capture;

let slider0;
let slider1;
let slider2;
let slider3;
let slider4;
let slider5;
let slider6;
let checkBox1;
let checkBox2;
let checkBox3;
let checkBox4;
let checkBox5;
let checkBox6;
let bkgColor;
let elementColor;
let displayColor;
let opacityColor;
let saveBtn;

// -- UI Controls

let panX;
let panY;
let panZ;
let pixelDepth;
let meshSize;
let fatStroke;

let saveImage;

let displayCircle = false;
let sliderGroup = [];

let myCanvas;

function setup() {
  myCanvas = createCanvas(480, 480, WEBGL);
  capture = createCapture(VIDEO);
  capture.size(640, 360);

  // -- Sliders settings
  slider0 = select('#stepSize');
  slider1 = select('#panX');
  slider2 = select('#panY');
  slider3 = select('#panZ');
  slider4 = select('#pixelDepth');
  slider5 = select('#meshSize');
  slider6 = select('#fatStroke');
  // -- Checkbox modes
  checkBox1 = select('#dot');
  checkBox2 = select('#lineX');
  checkBox3 = select('#lineY');
  checkBox4 = select('#quadMode');
  checkBox5 = select('#quadFill');
  checkBox6 = select('#cubeMode');
  // -- Custom colors
  bkgColor = select('#bkgColor');
  elementColor = select('#elementColor');
  displayColor = select('#displayColor');
  opacityColor = select('#opacityColor');
  // -- Capture image btn
  saveBtn = createButton('CAPTURE');
  saveBtn.addClass('capture-btn')
  saveBtn.mousePressed(saveImageNow);

  capture.hide();
  noStroke();
}

function draw() {
  const stepSize = slider0.value();
  panX = slider1.value();
  panY = slider2.value();
  panZ = slider3.value();
  pixelDepth = slider4.value();
  meshSize = slider5.value();
  fatStroke = slider6.value();
  checkboxDotMode = checkBox1.checked();
  checkboxCubeMode = checkBox6.checked();

  // --
  background(bkgColor.value());
  capture.loadPixels();
  strokeWeight(fatStroke);

  orbitControl();
  translate(panX, panY, panZ);

  // -- TEST CIRCLE POSITION
  if (displayCircle) {
    fill(200);
    circle(0, 0, 200);
  }

  push();
  translate(-150, 150, 0);
  for (let x = 0; x < capture.height; x += stepSize) {
    for (let y = 0; y < capture.width; y += stepSize) {
      const i = x * capture.width + y;
      const [r,g,b] = getRGB(i, capture.pixels);
      const darkness = (capture.pixels[i * 4]) / 2;
      const quad = getQuad(x, capture.width, y, stepSize);
      // rotateY(frameCount * 0.01);
      
      // -- DRAW RECT --
      if (checkBox4.checked()) {
        noStroke();
        beginShape();
        stroke(r,g,b);

        if (!checkBox5.checked()) {
          noFill();
        } else if (displayColor.checked()) {
          let [r, g, b] = elementColor.value().substring(1).convertToRGB();
          fill(r, g, b, opacityColor.value());
        } else {
          fill(r,g,b, opacityColor.value());
        }
        
        vertex(quad.y1, quad.x1, quad.z1);
        vertex(quad.y2, quad.x2, quad.z2);
        vertex(quad.y3, quad.x3, quad.z3);
        vertex(quad.y4, quad.x4, quad.z4);
        endShape();
      }
      
      // -- DRAW LINES X Y
      if (checkBox2.checked()) {
        if (displayColor.checked()) {
          stroke(elementColor.value());
        } else {
          stroke(r,g,b, opacityColor.value());
        }
        line(quad.y2, quad.x2, quad.z2 * pixelDepth, quad.y3, quad.x3, quad.z3 * pixelDepth);
      }

      if (checkBox3.checked()) {
        const quad = getQuad(x, capture.width, y, stepSize)
        if (displayColor.checked()) {
          stroke(elementColor.value());
        } else {
          stroke(r,g,b);
        }
        line(quad.y1, quad.x1, quad.z1 * pixelDepth, quad.y2, quad.x2, quad.z2 * pixelDepth);
      }

      // -- DRAW SPHERE --
      if (checkboxDotMode) {
        push();
        translate(quad.y2, quad.x2, quad.z2 * pixelDepth);
        if (displayColor.checked()) {
          let [r, g, b] = elementColor.value().substring(1).convertToRGB();
          fill(r, g, b, opacityColor.value());
        } else {
          fill(r,g,b, opacityColor.value());
        }
        noStroke();
        sphere(meshSize, 16, 16);
        pop();
      }

      // -- DRAW CUBE --
      if (checkboxCubeMode) {
        push();
        translate(quad.y2, quad.x2, quad.z2 * pixelDepth);
        if (displayColor.checked()) {
          let [r, g, b] = elementColor.value().substring(1).convertToRGB();
          fill(r, g, b, opacityColor.value());
        } else {
          fill(r,g,b, opacityColor.value());
        }
        noStroke();
        box(meshSize);
        pop();
      }
    }
  }
  pop();
  
}

// -- functions ------------------------------------------
function getRGB(i, img) {
  const r = img[i * 4];
  const g = img[i * 4 + 1];
  const b = img[i * 4 + 2];
  return [r,g,b];
}


function getQuad(x, width , y, step) {
  const depth = 4;
  const i1 = x * width + y;
  const [r1,g1,b1] = getRGB(i1, capture.pixels);
  const x1 = -capture.width / 2 + x;
  const y1 = -capture.height / 2 + y;
  const z1 = (capture.pixels[i1 * depth]) / 2 ;
  
  const i2 = (x + step) * width + y;
  const [r2,g2,b2] = getRGB(i2, capture.pixels);
  const x2 = -capture.width / 2 + x + step;
  const y2 = -capture.height / 2 + y;
  const z2 = (capture.pixels[i2 * depth]) / 2 ;
  
  const i3 = (x + step) * width + (y + step);
  const [r3,g3,b3] = getRGB(i3, capture.pixels);
  const x3 = -capture.width / 2 + x + step;
  const y3 = -capture.height / 2 + y + step;
  const z3 = (capture.pixels[i3 * depth]) / 2 ;

  const i4 = x * width + (y + step);
  const [r4,g4,b4] = getRGB(i4, capture.pixels);
  const x4 = -capture.width / 2 + x;
  const y4 = -capture.height / 2 + y + step;
  const z4 = (capture.pixels[i4 * depth]) / 2 ;

  return {i1, r1, g1, b1, x1, y1, z1, i2, r2, g2, b2, x2, y2, z2, i3, r3, g3, b3, x3, y3, z3, i4, r4, g4, b4, x4, y4, z4};
}

function saveImageNow() {
  saveCanvas(myCanvas, 'super-mega-face', 'jpg');
}
